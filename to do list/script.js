Vue.component('task',{
    props:['data'],
    methods:{
        done:function(){
            this.$emit('done');

        }
    },
    template:`
    <div class="task" >
        
        <div class="task__item-light">  
        
            <button @click="done()" class="add-task__btn-close add-task__btn-close--mod">x</button>
            <p class="task__block">Задача: <h3 class="task__name">{{data.title}}</h3></p>
        </div>
        </h3>
    </div>
    `
});


var vue=new Vue({
    el:'#app',
    data:{
        add_task:{
            title:'',
            desc:''
        },
        tasks:[
            
        ],
        isShowTR: false,
        darkTheme: false
    },
    methods:{
        delete_task(id){
            this.tasks.splice(id,1);
        },
        add(){
            if(this.add_task.title!=''){
                this.tasks.push({
                    title: this.add_task.title,
                    desc: this.add_task.desc
                });
                this.add_task.title="";
                this.add_task.desc="";
                
                this.isShowTR=false;
            }
        },
        openAdd(){
            this.isShowTR=true;
        },
        clear(){ 
            this.add_task.title="";
            this.add_task.desc="";
        }
    }
});